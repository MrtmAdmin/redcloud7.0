import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Common } from '../common/common';
import * as $ from 'jquery';

@Component({
    moduleId: module.id,
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit, AfterViewInit {

    constructor(private router: Router, private common: Common) { }

    ngOnInit(): void {
    }

    ngAfterViewInit(): void {
        this.changeHeight();
    }

    changeHeight() {
        var panelGot = this.common.winResize(null, null);
        $('#panelD').css('height', panelGot);
    }
    
     hasChanges() {
        return false;
    }
}