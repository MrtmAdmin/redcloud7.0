/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {CustomerContextRecords} from '../customercontextrecords/customer-context-records';
import {Configuration} from '../../services/configuration';
import {HttpService} from '../../services/http.service';
import {Common} from '../../common/common';

@Injectable()
export class CustomerContextRecordsService {
    
    private actionUrl: string;
    constructor(private http:HttpService,private configuration: Configuration,private common:Common) {
        this.actionUrl = configuration.ServerWithApiUrl;
    }
    
    get(): Promise<CustomerContextRecords[]> {
        return this.http
            .get(this.actionUrl +'ered/customercontextrecord')
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
    
}

