/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

export class DeploymentRequest{
    
    public deploymentRequestId:number;
    public packageId : string;
    public deploymentRequestName:string;
    public scheduleDeployment:string;
    public scheduleDeploymentDay:number;
    public scheduleDeploymentMonth:string;
    public scheduleDeploymentYear:number;
    public scheduleDeploymentHr:number;
    public scheduleDeploymentMin:number;
    public scheduleDeploymentMeridian:string;
    public approver:string;
    public comments:string;
    public status:string;
    public scheduledBy:string;
    public scheduledOn:string;
    public scheduledFor:string;
    
    constructor(){
        this.scheduleDeploymentDay = 1;
        this.scheduleDeploymentMonth = "Jan";
        this.scheduleDeploymentYear = 2016;
        this.scheduleDeploymentMeridian = "AM";
    }
}
