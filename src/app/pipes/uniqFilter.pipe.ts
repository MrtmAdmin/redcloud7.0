/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {Pipe, PipeTransform, Injectable} from '@angular/core';
declare var _: any; // lodash, not strictly typed

@Pipe({
    name: 'uniqFilter',
    pure: false
})

@Injectable()
export class UniquePipe implements PipeTransform {
    transform(items: any[], args: any[]): any {
        // lodash uniqBy function
        return _.uniqBy(items, args);
    }
}
