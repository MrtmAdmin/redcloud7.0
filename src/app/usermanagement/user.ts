/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {Role} from './role';
import {Feature} from './feature';

export class User {

    public userId: number;
    public fullName: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public password: string;
    public token: string;
    public roles: Role[];
    public userProfile: any;
    public features: Feature[];
    public screenPermissions: ScreenPermission[];

    constructor() {
        this.userId = null;
        this.roles = [];
        this.features = [];
        this.screenPermissions = [];
    };
}

export class ScreenPermission {

    public screenGroup: string;
    public screenName: string;
    public accessLevel: string;

    constructor() {}
}

