/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {Component, AfterViewInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Common} from '../../common/common';

@Component({
    selector: 'config-upload',
    templateUrl: './config.component.html'
})

export class ConfigUploadComponent implements AfterViewInit {
    
    featureLink:string;
    
    constructor(
        private route: ActivatedRoute,
        private common: Common) {
        this.featureLink = "http://192.168.1.199/cdt/";
    }

    ngAfterViewInit(): void {
        this.changeSizeFromViewPort();
    }

    changeSizeFromViewPort() {
        var panelGot = this.common.winResize('menuJ', null);
        $('.explore-frame').css('height', panelGot - 25);
    }

}





