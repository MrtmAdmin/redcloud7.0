import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Constant} from '../common/constant';
import {AuthService} from '../login/authentication.service';

@Component({
    selector: 'reset-password',
    templateUrl: './resetpassword.component.html'
})

export class ResetPasswordComponent implements OnInit {

    private userName: string;
    confirmPassword: string;
    error = '';
    isRequesting: boolean = false;
    changePasswordSuccess: boolean = false;
    resetMessage: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private constant: Constant,
        private authService: AuthService
    ) {}

    ngOnInit() {
        this.userName = this.route.queryParams['_value']['username']
    }

    resetPassword() {
        let user: any = {};
        user.userName = this.userName;
        user.password = this.confirmPassword;
        let ctrl = this;
        this.isRequesting = true;
        this.authService.resetPassword(user).then(function (successObj) {
            ctrl.isRequesting = false;
            ctrl.changePasswordSuccess = true;
            if (successObj.successDescription != undefined && successObj.successDescription != null && successObj.successDescription != "") {
                ctrl.resetMessage = successObj.successDescription;
            } else {
                ctrl.resetMessage = "Password Set Successfully";
            }
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.error = JSON.parse(error._body).error_description;
                ctrl.changePasswordSuccess = false;
            }
            ctrl.isRequesting = false;
        });
    }
    navigateToLogin() {
        let ctrl = this;
        ctrl.router.navigate([ctrl.constant.loginUrl]);
    }
}