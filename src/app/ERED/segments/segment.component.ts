/*
 * This is the Segment component which
 * binds the Segment HTML with the Segment service.
 * All the logic related to Segment are mentioned in this component file.
 * @Author : Shreya
 */
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Segments } from '../segments/segment';
import { SegmentService } from '../segments/segment.service';
import { Generic } from '../../ERED/eredgenericmodels//generic';
import { SegmentCcrField } from '../segments/segment';
import { SegmentCcrOperations } from '../segments/segment';
import { CustomerContextRecordsService } from '../customercontextrecords/customer-context-records.service';
import { ContextMenuService } from '../../directives/angular2-contextmenu/src/contextMenu.service';
import { DataTable } from 'primeng/components/datatable/datatable';
import { IMultiSelectOption, IMultiSelectSettings } from '../../directives/dropdown-multiselect/multiselect-dropdown';
import * as _ from "lodash";
import {ToasterService} from '../../services/toaster.service';

@Component({
    moduleId: module.id,
    selector: 'segments',
    templateUrl: 'segment.component.html',
})
export class SegmentComponent implements OnInit, AfterViewInit {

    @ViewChild('configTable')
    configTable: DataTable;

    @ViewChild('simulTable')
    simulTable: DataTable;

    @ViewChild('prodTable')
    prodTable: DataTable;

    @ViewChild('segmentForm') form;

    ccrList: string[] = [];
    segmentList: Segments[] = [];
    headers: any = {};
    isRequesting: boolean;
    behavioralDnas = [];
    dataSet: Generic[] = [];
    showMenu = false;
    showFilterPopup = false;
    searchFilterFlag = '';
    showViewOptionsPopup = false;
    showBdnaInfoPopup = false;
    operatorsList: any = [];
    editRowId: any;
    myOptions: IMultiSelectOption[] = [];
    configMenuOptions = [];
    mySettings: IMultiSelectSettings = {
        pullRight: false,
        enableSearch: true,
        checkedStyle: 'fontawesome',
        buttonClasses: 'select-btn-filter',
        selectionLimit: 0,
        closeOnSelect: false,
        autoUnselect: false,
        showCheckAll: true,
        showUncheckAll: true,
        dynamicTitle: true,
        dynamicTitleMaxItems: 3,
        maxHeight: '300px',
    };
    bdnaClientX: any = 0;
    bdnaClientY: any = 0;
    filterClientX: any = 0;
    filterClientY: any = 0;
    viewOptionClientX: any = 0;
    viewOptionClientY: any = 0;
    bdnaInfoClientX: any = 0;
    bdnaInfoClientY: any = 0;
    filterModels = { columns: [], rows: [], cells: '' };
    appliedfilterModels = { columns: [], rows: [], cells: '' };
    appliedSearchfilterModels = { columns: [], rows: [], cells: '' };
    viewOptionModel = { showObjective: false, showStatus: false, colorBlindSym: false, colorGrouping: false };
    appliedViewOptionModel = { showObjective: true, showStatus: true, colorBlindSym: false, colorGrouping: false };
    columnList: IMultiSelectOption[] = [];
    rowList: IMultiSelectOption[] = [];
    newSegment = new Segments();
    deleteSegmentdisplay: boolean = false;
    archiveSegmentDisplay: boolean = false;
    createSegmentdisplay: boolean = false;
    cancelSegmentdisplay: boolean = false;
    cancelChangesFlag: string = '';
    ruleBuilderSuppressionDisplay: boolean = false;
    popupIndex: number = -1;
    deleteIndex: number = -1;
    selectedTab: string = "produc";
    // rcw:comment Shreya | initialization of variable for list view
    productionSegmentList: Segments[] = [];
    simulationSegmentList: Segments[] = [];
    configurationSegmentList: Segments[] = [];
    criteriaExp: string = "";
    filter = { "group": { "operator": "AND", "rules": [] } };
    segmentObj = new Segments();
    showUpdateScreen: boolean = false;
    showEnlargedScreen: boolean = false;
    eventDetectorFlag: boolean = false;
    openAddPageFlag: boolean = false;
    showLargeListFlag: boolean = true;
    submittedFlag: boolean = false;
    configListMenuOptions = [];
    selectedIndex: any = -1;
    isGridView: boolean = false;
    selectedBdnas: any;
    gridSegmentListTabs = [{ id: 1, title: "Production", active: true, shortName: "produc" }, { id: 2, title: "Simulation", active: false, shortName: "simul" }, { id: 3, title: "Configuration", active: false, shortName: "config" }];
    listSegmentListTabs = [{ id: 1, title: "Production", active: true, shortName: "produc" }, { id: 2, title: "Simulation", active: false, shortName: "simul" }, { id: 3, title: "Configuration", active: false, shortName: "config" }];
    bdnaInfo: any = {};
    modalContent = new Segments();

    constructor(private segmentService: SegmentService, private contextMenuService: ContextMenuService, private customerContextRecordsService: CustomerContextRecordsService, private router: Router,private toasterService:ToasterService) { };

    hasChanges() {
        return this.form.dirty;
    }

    ngAfterViewInit() {
        $('body').css('overflow', '');
        function scanTable($table) {
            var m = [];
            $table.children("tr").each(function (y, row) {
                $(row).children("td, th").each(function (x, cell) {
                    var $cell = $(cell),
                        // rcw:update | 2 line: cant't compare non arithmetic value using pipe operator should cast to Number
                        cspan = Number($cell.attr("colspan")) | 0,
                        rspan = Number($cell.attr("rowspan")) | 0,
                        tx, ty;
                    cspan = cspan ? cspan : 1;
                    rspan = rspan ? rspan : 1;
                    for (; m[y] && m[y][x]; ++x);  //skip already occupied cells in current row
                    for (tx = x; tx < x + cspan; ++tx) {  //mark matrix elements occupied by current cell with true
                        for (ty = y; ty < y + rspan; ++ty) {
                            if (!m[ty]) {  //fill missing rows
                                m[ty] = [];
                            }
                            m[ty][tx] = true;
                        }
                    }
                    var pos = { top: y, left: x };
                    $cell.data("cellPos", pos);
                });
            });
        };

        /* plugin */
        // $.fn.cellPos = function (rescan) {
        //     var $cell = this.first(),
        //         pos = $cell.data("cellPos");
        //     if (!pos || rescan) {
        //         var $table = $cell.closest("table, thead, tbody, tfoot");
        //         scanTable($table);
        //     }
        //     pos = $cell.data("cellPos");
        //     return pos;
        // }

    }

    ngOnInit(): void {
        this.myOptions = [
            { id: "Option 1", name: 'Option 1' },
            { id: "Option 2", name: 'Option 2' },
        ];
        this.getAllCustomerContextRecords();
        this.getSegmentList();
        this.getHeaders();

        this.operatorsList.push({ name: "=", value: "=" });
        this.operatorsList.push({ name: "<", value: "<" });
        this.operatorsList.push({ name: ">", value: ">" });
        this.operatorsList.push({ name: "Not >", value: "Not >" });
        this.operatorsList.push({ name: "Not <", value: "Not <" });
        this.getAllDbSegments();
    }

    deColourGroupCell() {
        var k = 0; //counter variable
        var m = 0; //counter variable
        var table = document.getElementById('segmentTbl'); //All tables as a collecti
        var rows = table.getElementsByTagName('tr'); //grab all rows by table

        // rcw:important to know ra:todo ra:ToClean | in type script you are not allowed to change the type
        // var cell = {};
        var cell:any;
        for (k = 2; k < rows.length; k += 1) {
            //start looping through all the rows
            for (m = 0; m < rows[k].childNodes.length; m += 1) {
                //start looping through all of the row's children
                if (rows[k].childNodes[m].nodeName.toLowerCase() === 'td') {
                    cell = rows[k].childNodes[m]; //found a td element, alias to cell for easier use
                    cell.style.backgroundColor = "white";
                }
            }
        }
    }

    colourGroupCell() {
        let ctrl = this;
        'use strict';
        var i = 0; //counter variable
        var j = 0; //counter variable
        var k = 0; //counter variable
        var m = 0; //counter variable
        var n = 0; //counter variable
        var table = document.getElementById('segmentTbl'); //All tables as a collection.

        // rcw:important ra:todo | in type script you are not allowed to change the type
        // var rows = []; //table rows collection, needed to determine columns
        var rows:any; //table rows collection, needed to determine columns
        // var cells = []; //td collection
        var cells; //td collection
        // var header = []; //th collection
        var header; //th collection
        // var cell = {}; //used first as a Cell object (custom, see below) and then as a td (after all unique values by column have been determined

        var values = []; //array of Cell objects (custom, see below).
        var map = {};
        var columnColorMap = { 'column0': 'pink', 'column1': 'pink' };
        for (let i = 0; i < ctrl.ccrList.length; i++) {
            if ((i + 2) % 2 == 0) {
                columnColorMap['column' + (i + 2)] = 'pink';
            } else {
                columnColorMap['column' + (i + 2)] = 'purple';
            }
        }
        //       //columnColorMap holds the shade with which to color the column.
        var C = function () {
            //Cell Object, holds properties unique to the cells for coloring
            this.value = 0; //Cell value
            this.color = {
                'red': 255,
                'green': 255,
                'blue': 255
            }; //Cell color, determined by column
            this.shades = {
                'purple': [216, 201, 216],
                'pink': [248, 211, 222],

            }; //A quick way to determine the shade for the column. It holds color values to modify (and always in RGB order) or a null value if R, G, or B should be left alone.
            this.column = 0; //Column the cell is in, relative to the table it is in.
            this.darken = function (stepValue, hue) {
                //function that returns a new color, based on the hue that is passed in
                var decrement = 4;
                var i = 0;
                var ret = {
                    'red': 255,
                    'green': 255,
                    'blue': 255
                };
                if (!stepValue) {
                    stepValue = 0;
                }
                decrement = decrement * stepValue;
                for (i = 0; i < hue.length; i += 1) {
                    if (hue[i]) {
                        hue[i] = hue[i] - decrement;
                    }
                }
                if (hue[0]) {
                    ret.red = hue[0];
                }
                if (hue[1]) {
                    ret.green = hue[1];
                }
                if (hue[2]) {
                    ret.blue = hue[2];
                }
                return ret;
            };
            this.getHexBackgroundColorString = function () {
                //returns `rbg(val, val, val) as '#RRGGBB'
                var s = '';
                var red = this.color.red.toString(16);
                var green = this.color.green.toString(16);
                var blue = this.color.blue.toString(16);
                if (red.length < 2) {
                    red = '0' + red;
                }
                if (green.length < 2) {
                    green = '0' + green;
                }
                if (green.length < 2) {
                    blue = '0' + blue;
                }
                s = '#' + red + green + blue;
                return s.toUpperCase();
            };
        };
        var colHasValue = function (array, cell) {
            //loop through array, returns 'if cell.value && cell.column are found or otherwise'
            var i = 0;
            var found = false;
            for (i = 0; i < array.length; i += 1) {
                if (array[i].value === cell.value && array[i].column === cell.column) {
                    found = true;
                    i = array.length;
                }
            }
            return found;
        };
        var count = 0;
        var thCount = 0;
        header = table.getElementsByTagName('th');
        for (i = 0; i < header.length; i++) {
            var cols = $(header[i]).prop("colSpan");
            for (j = 0; j < cols; j++) {
                map[count++] = thCount;
            }
            thCount++;
        }


        // rcw:important ra:todo | in type script you are not allowed to change the type
        cells = table.getElementsByTagName('td'); //get all td elements per table
        for (j = 0; j < cells.length; j += 1) {
            var cell;
            cell = new C(); //grab a new Cell object
            cell.value = cells[j].innerText; //capture cell value
            cell.column = (<any>$(cells[j])).cellPos().left; //capture cell column
            if (!colHasValue(values, cell)) {
                //hasn't been previously stored yet, so darken according to column and store
                cell.color = cell.darken(j, cell.shades[columnColorMap['column' + map[cell.column].toString()]]);
                values.push(cell); //capture all unique values
            }
        }
        rows = table.getElementsByTagName('tr'); //grab all rows by table
        for (k = 2; k < rows.length; k += 1) {
            //start looping through all the rows
            for (m = 0; m < rows[k].childNodes.length; m += 1) {
                //start looping through all of the row's children
                if ((' ' + rows[k].childNodes[m].className + ' ').indexOf(' ' + 'data' + ' ') > -1 && rows[k].childNodes[m].nodeName.toLowerCase() === 'td') {
                    cell = rows[k].childNodes[m]; //found a td element, alias to cell for easier use
                    for (n = 0; n < values.length; n += 1) {
                        //loop through stored cell values
                        if (cell.innerText === values[n].value && map[(<any>$(cell)).cellPos().left] === map[values[n].column]) {
                            //value and column matches
                            cell.style.backgroundColor = values[n].getHexBackgroundColorString(); //set background-color
                            n = values.length; //exit for
                        }
                    }
                }
            }
        }
    }

    getSegmentList() {
        let seg = new Segments();
        seg.id = 1;
        seg.segmentName = "DATA TOPUP TACTICAL";
        seg.status = "Published";
        seg.version = 1.03;
        seg.isInProduction = true;
        let criterias = [];
        let criteria = {};
        let ccrObj = new SegmentCcrField();
        ccrObj.name = "Opted In Marketing";
        ccrObj.type = "text";
        let operation = {};
        let operator = new SegmentCcrOperations();
        operator.operator = "=";
        operator.value = "Yes";
        operation["="] = operator;
        ccrObj.operations = operation;
        criteria['Opted In Marketing'] = ccrObj;
        ccrObj = new SegmentCcrField();
        ccrObj.name = "Tenure (Days)";
        ccrObj.type = "select";
        operation = {};
        operator = new SegmentCcrOperations();
        operator.operator = "=";
        operator.value = "Yes";
        operation["="] = operator;
        operator = new SegmentCcrOperations();
        operator.operator = ">";
        operator.value = "1";
        operation[">"] = operator;
        ccrObj.operations = operation;
        criteria['Tenure (Days)'] = ccrObj;
        ccrObj = new SegmentCcrField();
        ccrObj.name = "P2P Propensity Score";
        ccrObj.type = "text";
        operation = {};
        operator = new SegmentCcrOperations();
        operator.operator = "=";
        operator.value = "No";
        operation["="] = operator;
        operator = new SegmentCcrOperations();
        ccrObj.operations = operation;
        criteria['P2P Propensity Score'] = ccrObj;
        ccrObj = new SegmentCcrField();
        ccrObj.name = "P2P Recency";
        ccrObj.type = "text";
        operation = {};
        operator = new SegmentCcrOperations();
        operator.operator = ">";
        operator.value = "63";
        operation[">"] = operator;
        ccrObj.operations = operation;
        criteria['P2P Recency'] = ccrObj;
        ccrObj = new SegmentCcrField();
        ccrObj.name = "Decay Decile";
        ccrObj.type = "text";
        operation = {};
        operator = new SegmentCcrOperations();
        operator.operator = "=";
        operator.value = "No";
        operation["="] = operator;
        operator = new SegmentCcrOperations();
        ccrObj.operations = operation;
        criteria['Decay Decile'] = ccrObj;
        ccrObj = new SegmentCcrField();
        ccrObj.name = "Tariff";
        ccrObj.type = "text";
        operation = {};
        operator = new SegmentCcrOperations();
        operator.operator = ">";
        operator.value = "96";
        operation[">"] = operator;
        ccrObj.operations = operation;
        criteria['Tariff'] = ccrObj;
        criterias.push(criteria);
        criteria = {};
        ccrObj = new SegmentCcrField();
        ccrObj.name = "Opted In Marketing";
        ccrObj.type = "text";
        operation = {};
        operator = new SegmentCcrOperations();
        operator.operator = "=";
        operator.value = "Yes";
        operation["="] = operator;
        ccrObj.operations = operation;
        criteria['Opted In Marketing'] = ccrObj;
        ccrObj = new SegmentCcrField();
        ccrObj.name = "Tenure (Days)";
        ccrObj.type = "select";
        operation = {};
        operator = new SegmentCcrOperations();
        operator.operator = "=";
        operator.value = "Yes";
        operation["="] = operator;
        operator = new SegmentCcrOperations();
        operator.operator = ">";
        operator.value = "1";
        operation[">"] = operator;
        ccrObj.operations = operation;
        criteria['Tenure (Days)'] = ccrObj;
        ccrObj = new SegmentCcrField();
        ccrObj.name = "P2P Propensity Score";
        ccrObj.type = "text";
        operation = {};
        operator = new SegmentCcrOperations();
        operator.operator = "=";
        operator.value = "No";
        operation["="] = operator;
        operator = new SegmentCcrOperations();
        ccrObj.operations = operation;
        criteria['P2P Propensity Score'] = ccrObj;
        ccrObj = new SegmentCcrField();
        ccrObj.name = "P2P Recency";
        ccrObj.type = "text";
        operation = {};
        operator = new SegmentCcrOperations();
        operator.operator = ">";
        operator.value = "63";
        operation[">"] = operator;
        ccrObj.operations = operation;
        criteria['P2P Recency'] = ccrObj;
        ccrObj = new SegmentCcrField();
        ccrObj.name = "Decay Decile";
        ccrObj.type = "text";
        operation = {};
        operator = new SegmentCcrOperations();
        operator.operator = "=";
        operator.value = "No";
        operation["="] = operator;
        operator = new SegmentCcrOperations();
        ccrObj.operations = operation;
        criteria['Decay Decile'] = ccrObj;
        ccrObj = new SegmentCcrField();
        ccrObj.name = "Tariff";
        ccrObj.type = "text";
        operation = {};
        operator = new SegmentCcrOperations();
        operator.operator = ">";
        operator.value = "96";
        operation[">"] = operator;
        ccrObj.operations = operation;
        criteria['Tariff'] = ccrObj;
        criterias.push(criteria);
        seg.criterias = criterias;
        this.rowList.push({ id: seg.id, name: seg.segmentName });
        this.segmentList.push(seg);
        //                seg = new Segments();
        //                seg.id = 2;
        //                seg.segmentName = "SERVICE_ONLY";
        //                seg.status = "Published";
        //                seg.version = 1.03;
        //                seg.isInProduction = true;
        //                criterias = [];
        //                criteria = {};
        //                ccrObj = new SegmentCcrField();
        //                ccrObj.name = "Opted In Marketing";
        //                ccrObj.type = "text";
        //                operation = {};
        //                operator = new SegmentCcrOperations();
        //                operator.operator = "=";
        //                operator.value = "Yes";
        //                operation["="] = operator;
        //                ccrObj.operations = operation;
        //                criteria['Opted In Marketing'] = ccrObj;
        //                ccrObj = new SegmentCcrField();
        //                ccrObj.name = "Tenure (Days)";
        //                ccrObj.type = "select";
        //                operation = {};
        //                operator = new SegmentCcrOperations();
        //                operator.operator = "=";
        //                operator.value = "Yes";
        //                operation["="] = operator;
        //                operator = new SegmentCcrOperations();
        //                operator.operator = ">";
        //                operator.value = "1";
        //                operation[">"] = operator;
        //                ccrObj.operations = operation;
        //                criteria['Tenure (Days)'] = ccrObj;
        //                ccrObj = new SegmentCcrField();
        //                ccrObj.name = "P2P Propensity Score";
        //                ccrObj.type = "text";
        //                operation = {};
        //                operator = new SegmentCcrOperations();
        //                operator.operator = "=";
        //                operator.value = "No";
        //                operation["="] = operator;
        //                operator = new SegmentCcrOperations();
        //                ccrObj.operations = operation;
        //                criteria['P2P Propensity Score'] = ccrObj;
        //                ccrObj = new SegmentCcrField();
        //                ccrObj.name = "P2P Recency";
        //                ccrObj.type = "text";
        //                operation = {};
        //                operator = new SegmentCcrOperations();
        //                operator.operator = ">";
        //                operator.value = "63";
        //                operation[">"] = operator;
        //                ccrObj.operations = operation;
        //                criteria['P2P Recency'] = ccrObj;
        //                ccrObj = new SegmentCcrField();
        //                ccrObj.name = "Decay Decile";
        //                ccrObj.type = "text";
        //                operation = {};
        //                operator = new SegmentCcrOperations();
        //                operator.operator = "=";
        //                operator.value = "No";
        //                operation["="] = operator;
        //                operator = new SegmentCcrOperations();
        //                ccrObj.operations = operation;
        //                criteria['Decay Decile'] = ccrObj;
        //                ccrObj = new SegmentCcrField();
        //                ccrObj.name = "Tariff";
        //                ccrObj.type = "text";
        //                operation = {};
        //                operator = new SegmentCcrOperations();
        //                operator.operator = ">";
        //                operator.value = "96";
        //                operation[">"] = operator;
        //                ccrObj.operations = operation;
        //                criteria['Tariff'] = ccrObj;
        //                criterias.push(criteria);
        //                seg.criterias = criterias;
        //                this.rowList.push({id: seg.id, name: seg.segmentName});
        //                this.segmentList.push(seg);
        //                seg = new Segments();
        //                seg.id = 3;
        //                seg.segmentName = "FIRST_10_DAYS";
        //                seg.status = "Published";
        //                seg.version = 1.03;
        //                seg.isInProduction = true;
        //                criterias = [];
        //                criteria = {};
        //                ccrObj = new SegmentCcrField();
        //                ccrObj.name = "Opted In Marketing";
        //                ccrObj.type = "text";
        //                operation = {};
        //                operator = new SegmentCcrOperations();
        //                operator.operator = "=";
        //                operator.value = "Yes";
        //                operation["="] = operator;
        //                ccrObj.operations = operation;
        //                criteria['Opted In Marketing'] = ccrObj;
        //                ccrObj = new SegmentCcrField();
        //                ccrObj.name = "Tenure (Days)";
        //                ccrObj.type = "select";
        //                operation = {};
        //                operator = new SegmentCcrOperations();
        //                operator.operator = "=";
        //                operator.value = "Yes";
        //                operation["="] = operator;
        //                operator = new SegmentCcrOperations();
        //                operator.operator = ">";
        //                operator.value = "1";
        //                operation[">"] = operator;
        //                ccrObj.operations = operation;
        //                criteria['Tenure (Days)'] = ccrObj;
        //                ccrObj = new SegmentCcrField();
        //                ccrObj.name = "P2P Propensity Score";
        //                ccrObj.type = "text";
        //                operation = {};
        //                operator = new SegmentCcrOperations();
        //                operator.operator = "=";
        //                operator.value = "No";
        //                operation["="] = operator;
        //                operator = new SegmentCcrOperations();
        //                ccrObj.operations = operation;
        //                criteria['P2P Propensity Score'] = ccrObj;
        //                ccrObj = new SegmentCcrField();
        //                ccrObj.name = "P2P Recency";
        //                ccrObj.type = "text";
        //                operation = {};
        //                operator = new SegmentCcrOperations();
        //                operator.operator = ">";
        //                operator.value = "63";
        //                operation[">"] = operator;
        //                ccrObj.operations = operation;
        //                criteria['P2P Recency'] = ccrObj;
        //                ccrObj = new SegmentCcrField();
        //                ccrObj.name = "Decay Decile";
        //                ccrObj.type = "text";
        //                operation = {};
        //                operator = new SegmentCcrOperations();
        //                operator.operator = "=";
        //                operator.value = "No";
        //                operation["="] = operator;
        //                operator = new SegmentCcrOperations();
        //                ccrObj.operations = operation;
        //                criteria['Decay Decile'] = ccrObj;
        //                ccrObj = new SegmentCcrField();
        //                ccrObj.name = "Tariff";
        //                ccrObj.type = "text";
        //                operation = {};
        //                operator = new SegmentCcrOperations();
        //                operator.operator = ">";
        //                operator.value = "96";
        //                operation[">"] = operator;
        //                ccrObj.operations = operation;
        //                criteria['Tariff'] = ccrObj;
        //                criterias.push(criteria);
        //                seg.criterias = criterias;
        //                this.rowList.push({id: seg.id, name: seg.segmentName});
        //                this.segmentList.push(seg);
        //        seg = new Segments();
        //        seg.id = 2;
        //        seg.segmentName = "SERVICE_ONLY";
        //        seg.status = "Published";
        //        seg.version = 1.03;
        //        seg.isInProduction = false;
        //        criteria = {};
        //        ccrObj = new SegmentCcrField();
        //        ccrObj.name = "Opted In Marketing";
        //        ccrObj.type = "text";
        //        operation = {};
        //        operator = new SegmentCcrOperations();
        //        operator.operator = "=";
        //        operator.value = "Yes";
        //        operation["="] = operator;
        //        ccrObj.operations = operation;
        //        criteria['Opted In Marketing'] = ccrObj;
        //        ccrObj = new SegmentCcrField();
        //        ccrObj.name = "Tenure (Days)";
        //        ccrObj.type = "select";
        //        operation = {};
        //        operator = new SegmentCcrOperations();
        //        operator.operator = "=";
        //        operator.value = "Yes";
        //        operation["="] = operator;
        //        operator = new SegmentCcrOperations();
        //        operator.operator = ">";
        //        operator.value = "96";
        //        operation[">"] = operator;
        //        ccrObj.operations = operation;
        //        criteria['Tenure (Days)'] = ccrObj;
        //        ccrObj = new SegmentCcrField();
        //        ccrObj.name = "P2P Propensity Score";
        //        ccrObj.type = "text";
        //        operation = {};
        //        operator = new SegmentCcrOperations();
        //        operator.operator = "=";
        //        operator.value = "No";
        //        operation["="] = operator;
        //        operator = new SegmentCcrOperations();
        //        ccrObj.operations = operation;
        //        criteria['P2P Propensity Score'] = ccrObj;
        //        ccrObj = new SegmentCcrField();
        //        ccrObj.name = "P2P Recency";
        //        ccrObj.type = "text";
        //        operation = {};
        //        operator = new SegmentCcrOperations();
        //        operator.operator = ">";
        //        operator.value = "63";
        //        operation[">"] = operator;
        //        ccrObj.operations = operation;
        //        criteria['P2P Recency'] = ccrObj;
        //        ccrObj = new SegmentCcrField();
        //        ccrObj.name = "Decay Decile";
        //        ccrObj.type = "text";
        //        operation = {};
        //        operator = new SegmentCcrOperations();
        //        operator.operator = "=";
        //        operator.value = "No";
        //        operation["="] = operator;
        //        operator = new SegmentCcrOperations();
        //        ccrObj.operations = operation;
        //        criteria['Decay Decile'] = ccrObj;
        //        ccrObj = new SegmentCcrField();
        //        ccrObj.name = "Tariff";
        //        ccrObj.type = "text";
        //        operation = {};
        //        operator = new SegmentCcrOperations();
        //        operator.operator = ">";
        //        operator.value = "96";
        //        operation[">"] = operator;
        //        ccrObj.operations = operation;
        //        criteria['Tariff'] = ccrObj;
        //        this.segmentList.push(seg);
        //        seg = new Segments();
        //        seg.id = 3;
        //        seg.segmentName = "FIRST_10_DAYS";
        //        seg.status = "Published";
        //        seg.version = 1.03;
        //        seg.isInProduction = false;
        //        this.segmentList.push(seg);
        //        seg = new Segments();
        //        seg.id = 4;
        //        seg.segmentName = "FIRST_90_DAYS";
        //        seg.status = "Published";
        //        seg.version = 1.03;
        //        seg.isInProduction = true;
        //        this.segmentList.push(seg);
        //        seg = new Segments();
        //        seg.id = 5;
        //        seg.segmentName = "PRE2POST";
        //        seg.status = "Published";
        //        seg.version = 1.03;
        //        seg.isInProduction = false;
        //        this.segmentList.push(seg);
        //        seg = new Segments();
        //        seg.id = 6;
        //        seg.segmentName = "HIGH_DECAY_RISK";
        //        seg.status = "Draft";
        //        seg.version = 1.03;
        //        seg.isInProduction = false;
        //        this.segmentList.push(seg);
        //        seg = new Segments();
        //        seg.id = 7;
        //        seg.segmentName = "STRETCHABLE";
        //        seg.status = "Published";
        //        seg.version = 1.03;
        //        seg.isInProduction = true;
        //        this.segmentList.push(seg);
        //        criteria = {};
        //        ccrObj = new SegmentCcrField();
        //        ccrObj.name = "Opted In Marketing";
        //        operation = {};
        //        operator = new SegmentCcrOperations();
        //        operator.operator = "=";
        //        operator.value = "No";
        //        operation["="] = operator;
        //        operator = new SegmentCcrOperations();
        //        operator.operator = ">";
        //        operator.value = "96";
        //        operation[">"] = operator;
        //        ccrObj.operations = operation;
        //        criteria['Opted In Marketing'] = ccrObj;
        //        ccrObj = new SegmentCcrField();
        //        ccrObj.name = "Tenure (Days)";
        //        criteria['Tenure (Days)'] = ccrObj;
        //        ccrObj = new SegmentCcrField();
        //        ccrObj.name = "P2P Propensity Score";
        //        criteria['P2P Propensity Score'] = ccrObj;
        //        ccrObj = new SegmentCcrField();
        //        ccrObj.name = "Offer Response Stretchable Recency";
        //        operation = {};
        //        operator = new SegmentCcrOperations();
        //        operator.operator = "=";
        //        operator.value = "No";
        //        operation["="] = operator;
        //        operator = new SegmentCcrOperations();
        //        operator.operator = ">";
        //        operator.value = "96";
        //        operation[">"] = operator;
        //        ccrObj.operations = operation;
        //        criteria['Offer Response Stretchable Recency'] = ccrObj;
        //        ccrObj = new SegmentCcrField();
        //        ccrObj.name = "Decay Decile";
        //        criteria['Decay Decile'] = ccrObj;
        //        ccrObj = new SegmentCcrField();
        //        ccrObj.name = "Tariff";
        //        criteria['Tariff'] = ccrObj;
    }

    // rcw:comment Shreya |  - Setting the current selected Tab
    setTabParameters(tabName, index) {
        if (index > -1) {
            for (let i = 0; i < this.listSegmentListTabs.length; i++) {
                if (i == index) {
                    this.listSegmentListTabs[i].active = true;
                } else {
                    this.listSegmentListTabs[i].active = false;
                }
            }
        }
        this.selectedTab = tabName;
    }

    searchSegments() {
        this.searchFilterFlag = "search";
        this.appliedSearchfilterModels = _.cloneDeep(this.filterModels);
        this.showFilterPopup = false;
        this.appliedfilterModels = { columns: [], rows: [], cells: '' };
    }

    openCreateModal() {
        this.createSegmentdisplay = true;
    }

    closeCreateModal() {
        this.createSegmentdisplay = false;
        this.newSegment = new Segments();
    }

    applyFilter() {
        this.searchFilterFlag = "filter";
        this.appliedfilterModels = _.cloneDeep(this.filterModels);
        this.showFilterPopup = false;
        this.appliedSearchfilterModels = { columns: [], rows: [], cells: '' };

    }

    reloadColorChanges() {
        if (this.appliedViewOptionModel.colorGrouping) {
            this.colourGroupCell();
        }
    }

    applyViewOptions() {
        this.appliedViewOptionModel = _.cloneDeep(this.viewOptionModel);
        if (this.appliedViewOptionModel.colorGrouping) {
            this.colourGroupCell();
        } else {
            this.deColourGroupCell();
        }
        this.showViewOptionsPopup = false;
    }

    openFilterPopup($event) {
        this.showFilterPopup = true;
        this.showViewOptionsPopup = false;
        this.showMenu = false;
        console.log("this.searchFilterFlag :" + this.searchFilterFlag);
        if (this.searchFilterFlag == "search") {
            this.filterModels = _.cloneDeep(this.appliedSearchfilterModels);
        } else {
            this.filterModels = _.cloneDeep(this.appliedfilterModels);
        }
        this.filterClientX = $event.clientX;
        this.filterClientY = $event.clientY;
        $('body').css('overflow', 'hidden');
    }
    openViewOptions($event) {
        this.showViewOptionsPopup = true;
        this.showFilterPopup = false;
        this.showMenu = false;
        this.viewOptionModel = _.cloneDeep(this.appliedViewOptionModel);
        this.viewOptionClientX = $event.clientX;
        this.viewOptionClientY = $event.clientY;
        $('body').css('overflow', 'hidden');
    }

    clearPopupObjects() {
        this.showFilterPopup = false;
        this.showViewOptionsPopup = false;
        this.showBdnaInfoPopup = false;
        this.showMenu = false;
        $('body').css('overflow', '');
    }
    setModelValue($event, segIndex, rowCriteriaIndex, ccrheader, data) {
        this.segmentList[segIndex].criterias[rowCriteriaIndex][ccrheader].operations[data].value = $event.join(", ");
    }

    toggle(headerId, opIndex, segIndex, rowCriteriaIndex, ccrheader, data) {
        this.editRowId = "" + segIndex + rowCriteriaIndex + headerId + opIndex;
        setTimeout(() => { $("#val" + segIndex + rowCriteriaIndex + headerId + opIndex).focus() });
    }

    changeState(id, e) {
        console.log("click")
        $("#" + id).next('ul').toggle();
        if ($("#" + id).next('ul').is(':visible')) {
            $("#" + id + " i:first-child").addClass("open-caret");
            $("#" + id + " i:first-child").removeClass("close-caret");
        } else {
            $("#" + id + " i:first-child").addClass("close-caret");
            $("#" + id + " i:first-child").removeClass("open-caret");
        }
        e.stopPropagation();
        e.preventDefault();
    }

    addNewAttrMenu($event, ccrheader) {
        console.log("event :", $event);
        if (ccrheader == "") {
            $('body').css('overflow', 'hidden');
            this.showMenu = true;
            this.bdnaClientX = $event.clientX;
            this.bdnaClientY = $event.clientY;
        }
    }

    setNewBdna() {
        let ctrl = this;
        if (this.segmentList.length > 0) {
            for (let i = 0; i < this.segmentList.length; i++) {
                for (let j = 0; j < this.segmentList[i].criterias.length; j++) {
                    let tempList = {};
                    Object.keys(this.segmentList[i].criterias[j]).forEach(function (keyop, index) {
                        if (keyop == "") {
                            //                            tempList[ctrl.selectedBdnas] = ctrl.selectedBdnas;
                            ctrl.segmentList[i].criterias[j][keyop].name = ctrl.selectedBdnas;
                            tempList[ctrl.selectedBdnas] = ctrl.segmentList[i].criterias[j][keyop];
                        } else {
                            tempList[keyop] = ctrl.segmentList[i].criterias[j][keyop];
                        }
                    });
                    this.segmentList[i].criterias[j] = tempList;
                }
            }
        }
        console.log("this.segmentList afer :" + JSON.stringify(this.segmentList));
        this.showMenu = false;
        $('body').css('overflow', '');
        this.getHeaders();
    }

    showBdnaInfo(bdnaHeader, $event) {
        console.log("$event :", bdnaHeader)
        if (bdnaHeader != undefined) {
            this.showBdnaInfoPopup = true;
            this.bdnaInfoClientX = $event.clientX;
            this.bdnaInfoClientY = $event.clientY;
            for (let item of this.dataSet) {
                console.log("inside if", item.name)
                if (item.name == bdnaHeader) {
                    console.log("inside if")

                    this.bdnaInfo = _.cloneDeep(item);
                }
            }
        }
    }

    getAllCustomerContextRecords() {
        this.isRequesting = true;
        var segComp = this;
        this.behavioralDnas = [];
        this.customerContextRecordsService.get().then(function (supp) {
            for (let item of supp) {
                if (segComp.behavioralDnas.indexOf(item.subGroupName) == -1) {
                    segComp.behavioralDnas.push(item.subGroupName);
                }
                let ccrObj = new Generic();
                ccrObj.name = item.businessName;
                ccrObj.parenttype = item.subGroupName;
                ccrObj.type = "BDNA";
                ccrObj.description = item.businessName;
                segComp.dataSet.push(ccrObj);
            }
        }).then(function () {
            segComp.isRequesting = false;
        }).catch(function () {
            segComp.isRequesting = false;
        });
    }

    arrangeHeaderSegments($event) {
        if (this.segmentList.length > 0) {
            for (let i = 0; i < this.segmentList.length; i++) {
                for (let j = 0; j < this.segmentList[i].criterias.length; j++) {
                    let ctrl = this;
                    let tempObj = {};
                    for (let ccr of this.ccrList) {
                        Object.keys(this.segmentList[i].criterias[j]).forEach(function (keyop) {
                            if (keyop == ccr) {
                                tempObj[ccr] = ctrl.segmentList[i].criterias[j][keyop];
                            }
                        });

                    }
                    ctrl.segmentList[i].criterias[j] = tempObj;
                }

            }
        }
    }
    changeSegmentData(header, opindex) {
        if (this.segmentList.length > 0) {
            for (let i = 0; i < this.segmentList.length; i++) {
                for (let j = 0; j < this.segmentList[i].criterias.length; j++) {
                    for (let key in this.segmentList[i].criterias[j]) {
                        if (key == header) {
                            let ctrl = this;
                            let tempList = {};
                            Object.keys(this.segmentList[i].criterias[j][key].operations).forEach(function (keyop, index) {
                                if (index == opindex && ctrl.segmentList[i].criterias[j][key].operations != undefined) {
                                    tempList[ctrl.headers[header][opindex]] = ctrl.segmentList[i].criterias[j][key].operations[keyop];
                                    ctrl.segmentList[i].criterias[j][key].operations[ctrl.headers[header][opindex]] = ctrl.segmentList[i].criterias[j][key].operations[keyop];
                                    tempList[ctrl.headers[header][opindex]].operator = ctrl.headers[header][opindex];
                                } else {
                                    tempList[keyop] = ctrl.segmentList[i].criterias[j][key].operations[keyop];
                                }
                            });
                            ctrl.segmentList[i].criterias[j][key].operations = tempList;
                            break;
                        }
                    }
                }

            }
        }

    }

    addNewSegment(segmentForm) {
        if (segmentForm.form.valid) {
            this.newSegment.status = "DRAFT";
            this.newSegment.version = 1.0;
            this.newSegment.isInProduction = false;
            let criterias = [];
            if (this.segmentList.length == 0) {
                if (!_.isEmpty(this.headers)) {
                    let headers = _.cloneDeep(this.headers);
                    criterias.push(headers);
                    this.newSegment.criterias = _.cloneDeep(criterias);
                } else {
                    criterias.push({});
                    this.newSegment.criterias = _.cloneDeep(criterias);
                }

            } else {
                let critlist = [];
                let crit = _.cloneDeep(this.segmentList[0].criterias[0]);
                for (let key in crit) {
                    if (crit[key].operations != undefined) {
                        for (let keyData in crit[key].operations) {
                            crit[key].operations[keyData].value = "";
                        }
                    }
                }
                critlist.push(crit);
                this.newSegment.criterias = _.cloneDeep(critlist);
            }
            this.segmentList.push(_.cloneDeep(this.newSegment));
            this.rowList.push({ id: this.newSegment.id, name: this.newSegment.segmentName });
        }
        console.log("this.segmentList :::" + JSON.stringify(this.segmentList));
        this.closeCreateModal();
    }

    duplicateSegment(oldSegment, segIndex) {
        console.log("segIndex :" + segIndex)
        if (oldSegment !== undefined) {
            var duplicatedSegment = _.cloneDeep(oldSegment);
            duplicatedSegment.segmentName = duplicatedSegment.segmentName + "- duplicate";
            this.segmentList.splice(segIndex + 1, 0, duplicatedSegment);
        }
    }

    openDeleteSegmentPopup(item, segIndex) {
        this.deleteSegmentdisplay = true;
        this.archiveSegmentDisplay = false;
        this.deleteIndex = segIndex;
    }

    openArchiveSegmentConfirmation(item, segIndex) {
        this.archiveSegmentDisplay = true;
        this.deleteSegmentdisplay = false;
        this.deleteIndex = segIndex;
    }

    // rcw:comment Shreya |  - Open Cancel Modal Confirmation Popup
    openCancelConfirmation(item, cancelChanges, index) {
        this.cancelSegmentdisplay = true;
        this.cancelChangesFlag = cancelChanges;
        this.popupIndex = index;
        this.modalContent = _.cloneDeep(item);
    }

    deleteSegment() {
        if (this.isGridView) {
            this.segmentList.splice(this.deleteIndex, 1);
        } else {

        }
        this.deleteSegmentdisplay = false;
        this.deleteIndex = -1;
    }

    archiveSegment() {
        if (this.isGridView) {
            this.segmentList.splice(this.deleteIndex, 1);
        } else {

        }
        this.archiveSegmentDisplay = false;
        this.deleteIndex = -1;
    }

    addNewAttribute(header) {
        if (this.segmentList.length > 0) {
            if (this.segmentList.length == 1 && this.segmentList[0].criterias.length == 0) {
                let ccrObj = new SegmentCcrField();
                ccrObj.name = "";
                ccrObj.type = "text";
                let operation = {};
                let operator = new SegmentCcrOperations();
                operator.operator = "";
                operator.value = "";
                operation[""] = operator;
                ccrObj.operations = operation;
                this.segmentList[0].criterias.push({ "": ccrObj });
                this.getHeaders();
            } else {
                if (this.segmentList[0].criterias[0][""] == undefined || this.segmentList[0].criterias[0][""] == null) {
                    //                $("#segmentTbl").colResizable({disable: true});
                    for (let i = 0; i < this.segmentList.length; i++) {
                        if (this.segmentList[i].criterias.length == 0) {
                            let ccrObj = new SegmentCcrField();
                            ccrObj.name = "";
                            ccrObj.type = "text";
                            let operation = {};
                            let operator = new SegmentCcrOperations();
                            operator.operator = "";
                            operator.value = "";
                            operation[""] = operator;
                            ccrObj.operations = operation;
                            this.segmentList[i].criterias.push({ "": ccrObj });
                        } else {
                            for (let j = 0; j < this.segmentList[i].criterias.length; j++) {
                                var tempCCr = {};
                                let ccrObj = new SegmentCcrField();
                                ccrObj.name = "";
                                ccrObj.type = "text";
                                let operation = {};
                                let operator = new SegmentCcrOperations();
                                operator.operator = "";
                                operator.value = "";
                                operation[""] = operator;
                                ccrObj.operations = operation;

                                if (header == "") {
                                    this.segmentList[i].criterias[j][""] = ccrObj;
                                } else {
                                    for (let key in this.segmentList[i].criterias[j]) {
                                        tempCCr[key] = this.segmentList[i].criterias[j][key];
                                        if (key == header) {
                                            tempCCr[""] = ccrObj;
                                        }
                                    }
                                    this.segmentList[i].criterias[j] = tempCCr;
                                }
                            }
                        }
                    }
                    this.getHeaders();
                } else {
                    this.toasterService.sendToaster('error', 'Segment', 'Blank CCR already exist');
                }
            }
        }
        console.log("this.segmentList :" + JSON.stringify(this.segmentList));
    }

    addNewOperator(headerName) {
        if (this.segmentList.length > 0) {
            var cnt = 0;
            for (let key in this.segmentList[0].criterias[0]) {
                if (key == headerName) {
                    if (this.segmentList[0].criterias[0][key].operations[""] != undefined && this.segmentList[0].criterias[0][key].operations[""] != null) {
                        cnt++;
                        break;
                    }
                }
            }
            if (cnt == 0) {
                for (let i = 0; i < this.segmentList.length; i++) {
                    for (let j = 0; j < this.segmentList[i].criterias.length; j++) {
                        for (let key in this.segmentList[i].criterias[j]) {
                            if (key == headerName) {
                                let operator = new SegmentCcrOperations();
                                operator.operator = "";
                                operator.value = "";
                                if (this.segmentList[i].criterias[j][key].operations == undefined) {
                                    this.segmentList[i].criterias[j][key].operations = {};
                                }
                                this.segmentList[i].criterias[j][key].operations[""] = operator;
                                break;
                            }
                        }
                    }

                }
                this.getHeaders();
            } else {
                this.toasterService.sendToaster('error', 'Segment', 'Blank Operator already exist');
            }
        }
    }

    deleteAttribute(headerName) {
        if (this.segmentList.length > 0) {
            for (let i = 0; i < this.segmentList.length; i++) {
                if (this.segmentList[i].criterias.length > 0) {
                    for (let j = 0; j < this.segmentList[i].criterias.length; j++) {
                        for (let key in this.segmentList[i].criterias[j]) {
                            if (key == headerName) {
                                delete this.segmentList[i].criterias[j][key];
                            }
                        }
                    }
                }
            }
            this.getHeaders();
        }
    }

    addOrDuplicateCriteria(data, criteriaIndex, segIndex, action) {
        let newCriteria = _.cloneDeep(data);
        if (action == 'add') {
            for (let key in newCriteria) {
                if (newCriteria[key].operations != undefined) {
                    for (let keyData in newCriteria[key].operations) {
                        newCriteria[key].operations[keyData].value = "";
                    }
                }
            }
        }
        this.segmentList[segIndex].criterias.splice(criteriaIndex + 1, 0, newCriteria);
    }

    deleteCriteria(data, criteriaIndex, segIndex) {
        if (this.segmentList[segIndex].criterias.length == 1) {
            for (let key in data) {
                if (data[key].operations != undefined) {
                    for (let keyData in data[key].operations) {
                        data[key].operations[keyData].value = "";
                    }
                }
            }
        } else {
            this.segmentList[segIndex].criterias.splice(criteriaIndex, 1);
        }
    }

    onRowClick(event, id) {
        console.log(event.target.outerText, id);
    }

    getHeaders() {
        this.ccrList = [];
        this.columnList = [];
        this.headers = {};
        for (let i = 0; i < this.segmentList.length; i++) {
            if (this.segmentList[i].criterias.length > 0) {
                for (let j = 0; j < this.segmentList[i].criterias.length; j++) {
                    for (let key in this.segmentList[i].criterias[j]) {
                        if (this.ccrList.indexOf(key) == -1) {
                            this.ccrList.push(key);
                            this.columnList.push({ id: key, name: key });
                        }
                        if (this.headers[key] == undefined || this.headers[key] == null) {
                            this.headers[key] = [];
                        }
                        if (this.segmentList[i].criterias[j][key].operations != undefined) {
                            for (let keyData in this.segmentList[i].criterias[j][key].operations) {
                                if (this.headers[key].indexOf(keyData) == -1) {
                                    this.headers[key].push(keyData);
                                }
                            }
                        }

                    }
                }

            }
        }
    }

    public onContextMenu($event: MouseEvent, item: any, attr: any, index1: number, index2: number): void {

        this.configMenuOptions = [];
        if (attr == "header") {
            this.configMenuOptions.push({
                html: () => 'Add new Attribute',
                click: (item, $event) => {
                    if (item == "temp~temp~temp") {
                        item = "";
                    }
                    this.addNewAttribute(item);
                }
            });
            this.configMenuOptions.push({
                html: () => 'Add new Operator',
                click: (item, $event) => {
                    if (item == "temp~temp~temp") {
                        item = "";
                    }
                    this.addNewOperator(item);
                }
            });
            this.configMenuOptions.push({
                html: () => 'Delete Attribute',
                click: (item, $event) => {
                    if (item == "temp~temp~temp") {
                        item = "";
                    }
                    this.deleteAttribute(item);
                }
            });
        } else if (attr == "segment") {
            console.log("seg")
            this.configMenuOptions.push({
                html: () => 'Add New Segment',
                click: (item, $event) => {
                    ctrl.openCreateModal();
                }
            });
            this.configMenuOptions.push({
                html: () => 'Duplicate Segment',
                click: (item, $event) => {
                    this.duplicateSegment(item, index2);
                }
            });
            this.configMenuOptions.push({
                html: () => 'Create New Version',
                click: (item, $event) => {
                    console.log("new version")
                }
            });
            this.configMenuOptions.push({
                html: () => 'Delete Segment',
                click: (item, $event) => {
                    this.openDeleteSegmentPopup(item, index2);
                }
            });
        } else if (attr == "cell") {
            this.configMenuOptions.push({
                html: () => 'Add Criteria',
                click: (item, $event) => {
                    this.addOrDuplicateCriteria(item, index1, index2, 'add');
                }
            });
            this.configMenuOptions.push({
                html: () => 'Duplicate Criteria',
                click: (item, $event) => {
                    this.addOrDuplicateCriteria(item, index1, index2, 'dup');
                }
            });
            this.configMenuOptions.push({
                html: () => 'Delete Criteria',
                click: (item, $event) => {
                    this.deleteCriteria(item, index1, index2);
                }
            });
        }
        let ctrl = this;
        this.contextMenuService.show.next({
            actions: ctrl.configMenuOptions,
            event: $event,
            item: item
        });
        $event.preventDefault();
    }

    // rcw:comment Shreya |  - Code for List view of segment starts.

    changeSegmentView() {
        this.isGridView = !this.isGridView;
    }
    getAllDbSegments() {
        this.isRequesting = true;
        var segmentComp = this;
        this.productionSegmentList = [];
        this.simulationSegmentList = [];
        this.configurationSegmentList = [];
        for (let item of this.segmentList) {
            if (item.isInProduction) {
                segmentComp.productionSegmentList.push(item);
            } else if (item.isInSimulation) {
                segmentComp.simulationSegmentList.push(item);
            }
            segmentComp.configurationSegmentList.push(item)
        }
    }

    cancelUpdateChanges() {
        Object.keys(this.form.form.controls).forEach(control => {
            this.form.form.controls[control].markAsPristine();
        });
        this.segmentObj = _.cloneDeep(this.modalContent);
        if (this.segmentObj.criteriaExpression != null && this.segmentObj.criteriaExpression != "" && this.segmentObj.criteriaExpression != undefined) {
            this.filter.group = JSON.parse(this.segmentObj.criteriaExpression);
            this.criteriaExp = this.computed(this.filter.group);
        }
        this.selectedIndex = this.popupIndex;
        this.showUpdateScreen = true;
        this.eventDetectorFlag = true;
        this.showLargeListFlag = false;
        $(".expand-div").removeClass("wide-div");
        $(".crud-div").css("display", "block");
        this.cancelSegmentdisplay = false;
    }

    rejectSegmentTraversalChanges() {
        if (this.selectedTab == "config") {
            delete this.configTable.selection;
            for (let item of this.configurationSegmentList) {
                if (item.id == this.segmentObj.id) {
                    this.configTable.selection = item;
                    break;
                }
            }
        } else if (this.selectedTab == "produc") {
            for (let item of this.productionSegmentList) {
                if (item.id == this.segmentObj.id) {
                    this.prodTable.selection = item;
                    break;
                }
            }
        } else if (this.selectedTab == "simul") {
            for (let item of this.simulationSegmentList) {
                if (item.id == this.segmentObj.id) {
                    this.simulTable.selection = item;
                    break;
                }
            }
        }
        this.cancelSegmentdisplay = false;
    }


    openUpdateTemplate(seg) {
        const segment = seg.data;
        let index = -1;
        // Retrieving the index of the suppression by identifying the object from the server suppression list
        if (this.selectedTab === 'config') {
            index = this.configurationSegmentList.indexOf(segment);
        } else if (this.selectedTab === 'produc') {
            index = this.productionSegmentList.indexOf(segment);
        } else if (this.selectedTab === 'simul') {
            index = this.simulationSegmentList.indexOf(segment);
        }

        // If object is present returns the index from the list and populates the suppression data onto the below config space
        if (index !== -1) {
            if (this.form.dirty) {
                this.openCancelConfirmation(segment, 'changeEditCancel', index);
            } else {
                Object.keys(this.form.form.controls).forEach(control => {
                    this.form.form.controls[control].markAsPristine();
                });
                this.segmentObj = _.cloneDeep(segment);
                if (this.segmentObj.criteriaExpression != null && this.segmentObj.criteriaExpression != "" && this.segmentObj.criteriaExpression != undefined) {
                    this.filter.group = JSON.parse(this.segmentObj.criteriaExpression);
                    this.criteriaExp = this.computed(this.filter.group);
                }
                this.selectedIndex = index;
                this.showUpdateScreen = true;
                this.eventDetectorFlag = true;
                this.showLargeListFlag = false;
                $(".expand-div").removeClass("wide-div");
                $(".crud-div").css("display", "block");
            }
        }
    }

    openAddPage() {
        if (!this.showUpdateScreen) {
            this.openAddPageFlag = true;
            this.showEnlargedScreen = true;
            this.showUpdateScreen = true;
            $(".crud-div").animate({ height: "toggle" });
            $("#tools").removeClass("fix-height-385").addClass("h-555");
        }
        this.clearObjects();
        this.showUpdateScreen = true;
    }

    setLargeListFlag() {
        if (this.showLargeListFlag) {
            this.showLargeListFlag = false;
            this.showEnlargedScreen = false;
            this.showUpdateScreen = true;
        } else {
            this.showLargeListFlag = true;
            this.showUpdateScreen = false;
        }
    }

    togglePage() {
        if (this.showEnlargedScreen) {
            this.showEnlargedScreen = false;
            this.openAddPageFlag = false;
        } else {
            this.showEnlargedScreen = true;
        }
        $(".expand-div").removeClass("wide-div");
    }

    ongroupChanged(val) {
        Object.assign(this.filter.group, val);
        this.criteriaExp = this.computed(this.filter.group);
        this.segmentObj.criteriaExpression = JSON.stringify(this.filter.group);
    }

    computed(group) {
        if (!group) return "";
        for (var str = "(", i = 0; i < group.rules.length; i++) {
            i > 0 && (str += " <strong>" + group.operator + "</strong> ");
            str += group.rules[i].group ?
                this.computed(group.rules[i].group) :
                group.rules[i].field + " " + this.htmlEntities(group.rules[i].condition) + " " + group.rules[i].data;
        }

        return str + ")";
    }

    htmlEntities(str) {
        return String(str).replace(/</g, '&lt;').replace(/>/g, '&gt;');
    }

    checkCancelChanges() {
        if (this.selectedIndex !== -1) {
            this.openCancelConfirmation(this.segmentObj, 'changeUsecaseCancel', -1);
        } else {
            //If it is newly created usecase it clears the form
            this.clearObjects();
        }
    }


    // rcw:comment Shreya |  - Cancels the changes on the usecase
    cancelSegments() {
        //If the segment is for updation cancels the new added changes
        if (this.selectedIndex !== -1) {
            if (this.selectedTab == "config") {
                this.segmentObj = _.cloneDeep(this.configurationSegmentList[this.selectedIndex]);
            } else if (this.selectedTab == "simul") {
                this.segmentObj = _.cloneDeep(this.simulationSegmentList[this.selectedIndex]);
            } else if (this.selectedTab == "produc") {
                this.segmentObj = _.cloneDeep(this.productionSegmentList[this.selectedIndex]);
            }
            this.cancelSegmentdisplay = false;
        } else {
            //If it is newly created segment it clears the form
            this.clearObjects();
        }
    }

    clearObjects() {
        Object.keys(this.form.form.controls).forEach(control => {
            this.form.form.controls[control].markAsPristine();
        });
        this.segmentObj = new Segments();
        this.filter = { "group": { "operator": "AND", "rules": [] } };
        if (this.configTable != null && this.configTable.selection != null) {
            delete this.configTable.selection;
        }
        if (this.simulTable != null && this.simulTable.selection != null) {
            delete this.simulTable.selection;
        }
        if (this.prodTable != null && this.prodTable.selection != null) {
            delete this.prodTable.selection;
        }
        this.submittedFlag = false;
        this.selectedIndex = -1;
        this.criteriaExp = "";
    }

    onSubmit(segmentForm) {
        this.submittedFlag = true;
        if (segmentForm.form.valid && (this.segmentObj.criteriaExpression && this.segmentObj.criteriaExpression != null && this.segmentObj.criteriaExpression != "")) {
            this.isRequesting = true;
            this.submittedFlag = false;
            if (this.segmentObj.criteriaExpression && this.segmentObj.criteriaExpression != null) {
                this.segmentObj.criteriaExpression = this.segmentObj.criteriaExpression.replace(/<\/?[^>]+(>|$)/g, "");
            }
            console.log("this.segmentObj :" + JSON.stringify(this.segmentObj));
            if (this.segmentObj.id) {
                delete this.segmentObj['_$visited'];
                var ctrl = this;
                this.segmentService.update(this.segmentObj).then(function (updateSeg) {
                this.toasterService.sendToaster('success', 'Segment', 'Segment updated successfully');
                    if (ctrl.selectedIndex == -1) {
                        ctrl.getAllDbSegments();
                    } else {
                        if (ctrl.selectedTab == "config") {
                            for (let i = 0; i < ctrl.configurationSegmentList.length; i++) {
                                if (ctrl.configurationSegmentList[i].id == updateSeg.id) {
                                    ctrl.configurationSegmentList[i] = updateSeg;
                                    break;
                                }
                            }
                        } else if (ctrl.selectedTab == "simul") {
                            for (let i = 0; i < ctrl.simulationSegmentList.length; i++) {
                                if (ctrl.simulationSegmentList[i].id == updateSeg.id) {
                                    ctrl.simulationSegmentList[i] = updateSeg;
                                    break;
                                }
                            }
                        } else if (ctrl.selectedTab == "produc") {
                            for (let i = 0; i < ctrl.productionSegmentList.length; i++) {
                                if (ctrl.productionSegmentList[i].id == updateSeg.id) {
                                    ctrl.productionSegmentList[i] = updateSeg;
                                    break;
                                }
                            }
                        }
                    }
                }).then(function () {
                    ctrl.clearObjects();
                    ctrl.isRequesting = false;
                }).catch(function (error) {
                    if (error && error._body) {
                        ctrl.toasterService.sendToaster('error', 'Segment', JSON.parse(error._body).error_description);  
                  } else {
                        ctrl.toasterService.sendToaster('error', 'Segment','Segment updation failed' );  
                    }
                    ctrl.isRequesting = false;
                });
            } else {
                this.segmentObj.id = null;
                var ctrl = this;
                this.segmentService.add(this.segmentObj).then(function (addedSeg) {
                    ctrl.toasterService.sendToaster('success', 'Segment', 'Segment creation successfully' );
                    if (ctrl.selectedTab == "config") {
                        ctrl.configurationSegmentList.push(addedSeg);
                    } else if (ctrl.selectedTab == "simul") {
                        ctrl.simulationSegmentList.push(addedSeg);
                    } else if (ctrl.selectedTab == "produc") {
                        ctrl.productionSegmentList.push(addedSeg);
                    }
                }).then(function () {
                    ctrl.clearObjects();
                    ctrl.isRequesting = false;
                }).catch(function (error) {
                    if (error && error._body) {
                        ctrl.toasterService.sendToaster('error', 'Segment', JSON.parse(error._body).error_description); 
                   } else {
                       ctrl.toasterService.sendToaster('error', 'Segment', 'Segment creation failed' );
                    }
                    ctrl.isRequesting = false;
                });

            }
        }
    }

    public onListContextMenu($event: MouseEvent, item: any): void {
        this.configListMenuOptions = [];
        this.configListMenuOptions.push({
            html: () => 'Version',
            click: (item) => {
                var ctrl = this;
                this.isRequesting = true;
                this.segmentService.nextVersion(item.id).then(function (newVersionSupp) {
                    ctrl.toasterService.sendToaster('success', 'Segment', 'New Version created successfuly' );
                    if (ctrl.selectedTab == "config") {
                        ctrl.configurationSegmentList.push(newVersionSupp);
                    } else if (ctrl.selectedTab == "simul") {
                        ctrl.simulationSegmentList.push(newVersionSupp);
                    } else if (ctrl.selectedTab == "produc") {
                        ctrl.productionSegmentList.push(newVersionSupp);
                    }
                }).then(function () {
                    ctrl.clearObjects();
                    ctrl.isRequesting = false;
                }).catch(function (error) {
                    if (error && error._body) {
                        ctrl.toasterService.sendToaster('error', 'Segment', JSON.parse(error._body).error_description);
                    } else {
                        ctrl.toasterService.sendToaster('error', 'Segment', 'New version creation failed');
                    }
                    ctrl.isRequesting = false;
                });
            },
            enabled: (item): boolean => {
                return item.versionable;
            }
        })
        this.configListMenuOptions.push({
            html: () => 'Duplicate',
            click: (item) => {
                console.log("Duplicate called");
                var ctrl = this;
                this.isRequesting = true;
                this.segmentService.copy(item.id).then(function (duplicateSupp) {
                    ctrl.toasterService.sendToaster('success', 'Segment', 'Segment duplicated successfully' );
                    if (ctrl.selectedTab == "config") {
                        ctrl.configurationSegmentList.push(duplicateSupp);
                    } else if (ctrl.selectedTab == "simul") {
                        ctrl.simulationSegmentList.push(duplicateSupp);
                    } else if (ctrl.selectedTab == "produc") {
                        ctrl.productionSegmentList.push(duplicateSupp);
                    }
                }).then(function () {
                    ctrl.clearObjects();
                    ctrl.isRequesting = false;
                }).catch(function (error) {
                    if (error && error._body) {
                        ctrl.toasterService.sendToaster('error', 'Segment', JSON.parse(error._body).error_description);
                    } else {
                        ctrl.toasterService.sendToaster('error', 'Segment', 'Segment duplication failed' );
                    }
                    ctrl.isRequesting = false;
                });
            }, enabled: (item): boolean => {
                return item.copyable;
            }
        })
        this.configListMenuOptions.push({
            html: () => 'Archive',
            click: (item) => {
                console.log("Archive called");
                this.openArchiveSegmentConfirmation(item, -1);
            }, enabled: (item): boolean => {
                return item.archivable;
            }
        })
        this.configListMenuOptions.push({
            html: () => 'Delete',
            click: (item) => {
                this.openDeleteSegmentPopup(item, -1);
            }, enabled: (item): boolean => {
                return item.deletable;
            }
        })
        this.contextMenuService.show.next({
            actions: this.configListMenuOptions,
            event: $event,
            item: item
        });
        $event.preventDefault();
    }

}
