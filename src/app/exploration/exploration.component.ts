import {Component, AfterViewInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Common} from '../common/common';

@Component({
    selector: 'bdna-exploration',
    templateUrl: './exploration.component.html'
})

export class ExplorationComponent implements AfterViewInit {

    currentFeature: String = "Default";
    featureLink: String = "";

    constructor(
        private route: ActivatedRoute,
        private common: Common) {
        this.route.params.subscribe(params => {
            this.currentFeature = params['featureName'];
            if(this.currentFeature == "BDNA"){
                this.featureLink = "https://dev-qlik1.dev-red-cloud.com/sense/app/167887cc-f24e-4561-9323-d3bd350d651f/sheet/HWRp/state/analysis";
            }else if(this.currentFeature == "Drop-off And Offers"){
                this.featureLink = "https://dev-qlik1.dev-red-cloud.com/sense/app/ea71e498-3165-4655-91bd-26f64847f934/sheet/314fb1f9-42b4-4246-b67e-f5a93fd7c3a0/state/analysis";
            }else if(this.currentFeature == "Drop-off And Offers (Sim)"){
                this.featureLink = "https://dev-qlik1.dev-red-cloud.com/sense/app/28adf723-9dd9-49b1-9ac0-15d09312d921/sheet/314fb1f9-42b4-4246-b67e-f5a93fd7c3a0/state/analysis";
            }else if(this.currentFeature == "Campaign Metrics"){
                this.featureLink = "https://dev-qlik1.dev-red-cloud.com/sense/app/0930f825-b1b6-4c95-b168-cc607e462961/sheet/HWRp/state/analysis";
            }else if(this.currentFeature == "Performance"){
                this.featureLink = "https://dev-qlik1.dev-red-cloud.com/sense/app/7fd2d5bb-6c85-4ee2-ac32-a5e688581b4f/sheet/PubYLfT/state/analysis";
            }
        });
    }

    ngAfterViewInit(): void {
        this.changeSizeFromViewPort();
    }

    changeSizeFromViewPort() {
        var panelGot = this.common.winResize('menuJ', null);
        $('.explore-frame').css('height', panelGot - 25);
    }

}



