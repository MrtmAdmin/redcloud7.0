/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {Feature} from './feature';
import {User} from './user';

export class Role {

    public roleId: number;
    public roleName: string;
    public selected: boolean;
    public roleScreens:Feature[];
    public users:User[];

    constructor() {
        this.selected = false;
        this.roleScreens = [];
    };
}
