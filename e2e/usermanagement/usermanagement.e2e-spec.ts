import {browser, element, by} from 'protractor';
import {UserManagementWebPage} from './usermanagement.po';
import {protractor} from 'protractor';
describe('emagineweb App - User Management Module', () => {

    let page: UserManagementWebPage;

    beforeEach(() => {
        page = new UserManagementWebPage();
    });

    // rcw:comment Shreya |  - Goes to Admin Page - test scenario
    it('Should broswer to Admin Page', () => {
        //TODO : Click Admin from menu and then goes to admin page
        browser.get('/#/users');
        browser.waitForAngular();
        browser.sleep(1000);
    });

    // rcw:comment Shreya |  - Click on add new user - test scenario
    it('Click the add new user button', () => {
        var EC = protractor.ExpectedConditions;
        var addNewUserBtn = page.getAddUserBtn();
        browser.wait(EC.visibilityOf(addNewUserBtn), 9000);
        addNewUserBtn.click();
    });

    // rcw:comment Shreya |  - Fill New User Details - test scenario
    it('Fill details of new user', () => {
        //Get Elements - of firstName,lastName,emailId, role i.e. HEAD_OF_CVM
        //only HEAD_OF_CVM because new user will have only that role
        var firstName = page.getFirstNamField();
        var lastName = page.getLastNameField();
        var emailId = page.getEmailField();
        var role1 = page.getRole1CheckBox();
        //Filling Details
        firstName.sendKeys(browser.params.newUserFirstNameValue);
        lastName.sendKeys(browser.params.newUserLastNameValue);
        emailId.sendKeys(browser.params.newUserEmailIdValue);
        role1.click();
        browser.sleep(1000);
    });

    // rcw:comment Shreya |  - Adding New User
    it('Adding new user', () => {
        //Clicking on save button
        var saveBtn = page.getSaveBtn();
        saveBtn.click();
        browser.sleep(10000);
        browser.waitForAngular();
        //Checking if the user is added of not
        //By checking if the user is there in HEAD_OF_CVM role (i.e. User is present in its role context menu or not) 
        var addedUserId = 'role0' + browser.params.newUserFirstNameValue;
        let addedUser = element(by.id(addedUserId));
        browser.wait(function () {
            return browser.isElementPresent(by.id(addedUserId));
        }, 5000);
        expect(addedUser.isPresent()).toBeTruthy();
    });

    // rcw:comment Shreya |  - Click on the user just created to edit - test scenario
    it('Click on test user to edit ', () => {
        browser.waitForAngular();
        var EC = protractor.ExpectedConditions;
        //Getting HEAD_OF_CVM element i.e. first role in left context menu
        var role0 = element(by.id('roles0'));
        browser.wait(EC.visibilityOf(role0), 5000);
        role0.click();
        //Getting the text user element in left context menu  
        var existingUser = element(by.id('role0' + browser.params.newUserFirstNameValue));
        //First Clicking on HEAD_OF_CVM and then once sub menu (i.e. list of users) is opened
        //Clicking on Test User
        browser.wait(EC.visibilityOf(existingUser), 5000);
        existingUser.click();
        browser.waitForAngular();
    });

    // rcw:comment Shreya |  - Click on Edit Details Button
    it('Click on edit details button', () => {
        var editDetails = page.getEditBtn();
        var EC = protractor.ExpectedConditions;
        browser.wait(EC.visibilityOf(editDetails), 5000);
        editDetails.click();
        browser.waitForAngular();
    });


    // rcw:comment Shreya |  - Filling new details of test user
    it('Fill new details of test user', () => {
        //Getting Elements - firstname,lastname, role - HEAD_OF_CVm and REPORTING_ANALYST
        //Chaning role of test user from HEAD_OF_CVM to REPORTING_ANALYST
        var firstName = page.getFirstNamField();
        var lastName = page.getLastNameField();
        var featurerole1 = page.getRole1CheckBox();
        var featurerole2 = page.getRole2CheckBox();
        firstName.clear();
        firstName.sendKeys(browser.params.upadteUserFirstNameValue);
        lastName.clear();
        lastName.sendKeys(browser.params.updateUserLastNameValue);
        featurerole1.click();
        featurerole2.click();
    });

    // rcw:comment Shreya |  - Save new user details of test user and verify
    it('Save new user details of test user and verify', () => {
        //Getting the save button and clicking it
        browser.driver.manage().window().maximize();
        var savebtn = page.getSaveBtn();
        var EC = protractor.ExpectedConditions;
        browser.executeScript("arguments[0].scrollIntoView();", savebtn.getWebElement());
        browser.wait(EC.visibilityOf(savebtn), 5000);
        savebtn.click();
        browser.waitForAngular();
        //Verfying that User has been updated
        //Now since Test User is in REPORTING_ANALYST menu 
        //Getting REPORTING_ANALYST menu and clicking on it
        var role2 = element(by.id('roles1'));
        browser.wait(EC.visibilityOf(role2), 5000);
        role2.click();
        //Clicking on our updated user
        var upadteUser = element(by.id('role1' + browser.params.upadteUserFirstNameValue));
        browser.wait(EC.visibilityOf(upadteUser), 5000);
        upadteUser.click();
        browser.waitForAngular();
        //Now checking if firstName,lastName and features are updated
        var editDetails = page.getEditBtn();
        browser.wait(EC.visibilityOf(editDetails), 5000);
        editDetails.click();
        browser.waitForAngular();
        var firstName = page.getFirstNamField();
        var lastName = page.getLastNameField();
        expect(firstName.getAttribute('value')).toBe(browser.params.upadteUserFirstNameValue);
        expect(lastName.getAttribute('value')).toBe(browser.params.updateUserLastNameValue);
        //Since HEAD_OF_CVM is unchecked
        expect(element(by.id('userRole0')).isSelected()).toBeFalsy();
        //Since REPORTING_ANALYST is checked
        expect(element(by.id('userRole1')).isSelected()).toBeTruthy();
        var cancelBtn = element(by.id('cancelBtn'));
        browser.executeScript("arguments[0].scrollIntoView();", cancelBtn.getWebElement());
        browser.wait(EC.visibilityOf(cancelBtn), 5000);
        cancelBtn.click();
        browser.waitForAngular();
    });

    // rcw:comment Shreya |  - Delete the Test user
    it('Delete the Test User', function (done) {
        browser.get('/#/users');
        var deleteUserFirstName = browser.params.upadteUserFirstNameValue;
        //Now the role assign is role2 i.e. REPORTING_ANALYST so opening the from left panel
        var role2 = element(by.id('roles1'));
        var EC = protractor.ExpectedConditions;
        browser.wait(EC.visibilityOf(role2), 5000);
        role2.click();
        //Finding our user in sub menu of REPORTING_ANALYST
        browser.actions().mouseMove(element(by.id('contextmenu' + deleteUserFirstName))).perform();
        //Right Clicking to get the delete option
        browser.actions().click(protractor.Button.RIGHT).perform();
        //Finding Delete element
        var menu = element(by.css(".user-manage"));
        var deleteUser = menu.element(by.tagName('a'));
        browser.actions().mouseMove(deleteUser).perform();;
        browser.actions().click().perform();
        browser.waitForAngular();
        //Finding the Yes Button 
        var yesBtn = element(by.id('yesBtn'));
        browser.executeScript('arguments[0].click();', yesBtn.getWebElement());
        browser.waitForAngular();
        //browser.get('/#/users');
        //browser.waitForAngular();
        //Verifying if actually delete or not
        browser.wait(EC.visibilityOf(role2), 5000);
        expect(role2.isDisplayed()).toBeTruthy();
        role2.click();
        // browser.sleep(3000);
        var updateUser1 = element(by.id('role1' + deleteUserFirstName));
        // expect(updateUser1.isPresent()).toBeFalsy();
        expect(browser.isElementPresent(updateUser1)).toBe(false);
        done();
    });
});