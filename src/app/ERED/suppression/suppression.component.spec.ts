/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BaseRequestOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule, BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_BASE_HREF } from '@angular/common';
import { SuppressionComponent } from './suppression.component';
import { SharedModule } from '../../shared/shared.module';
import { SuppressionService } from './suppression.service';
import { SuppressionMockDataService } from './suppression.mockdata.service';
import { CustomerContextRecordsService } from '../customercontextrecords/customer-context-records.service';
import { Configuration } from '../../services/configuration';
import { SuppressionMockBackendProvider } from './suppression.mockbackend';
import { Common } from '../../common/common';
import { Constant } from '../../common/constant';
import * as _ from 'lodash';
describe('SuppressionComponent', () => {
    let suppressionCompInstance: SuppressionComponent;
    let fixture: ComponentFixture<SuppressionComponent>;
    let suppressionMockDataService = new SuppressionMockDataService();
    beforeAll(function () {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 999999;
    });
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                NoopAnimationsModule,
                BrowserAnimationsModule,
                SharedModule,
                BrowserModule,
            ],
            declarations: [
                SuppressionComponent,
            ],
            providers: [
                SuppressionService,
                Configuration,
                MockBackend,
                CustomerContextRecordsService,
                SuppressionMockBackendProvider,
                BaseRequestOptions,
                Common,
                Constant,
                { provide: APP_BASE_HREF, useValue: '/' }
            ]
        }).compileComponents();

    }));

    //rcw:comment Shreya | -To check whether the component exists or not
    it('should create the app', async(() => {
        console.log("Create Suppression Component");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        expect(suppressionCompInstance).toBeTruthy();
    }));

    //rcw:comment Shreya | -To check the click of the tab and select the tab wise Suppression display
    it('Set Tab Parameter', async(() => {
        console.log("Set Tab Parameter In Suppression");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        suppressionCompInstance.setTabParameters('config', 2);
        expect(suppressionCompInstance.selectedTab).toEqual('config');
    }));

    //rcw:comment Shreya | -Compute the rule expression based on any given rule json 
    it('Computing Rule Expression', async(() => {

        console.log("Computing Rule Expression In Suppression");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        let str = suppressionCompInstance.computed({ "operator": "AND", "rules": [{ "condition": "=", "field": "Temp Name - -1524061161", "data": "36" }, { "condition": "=", "field": "supp-update", "data": "96" }] });
        expect(str).toEqual('(Temp Name - -1524061161 = 36 <strong>AND</strong> supp-update = 96)');
    }));

    //rcw:comment Shreya | -Add Suppressions creating the event detector object from mockbackend
    // and calling the add function
    // from the component.
    it('Add Suppressions', function (done) {
        console.log("Add Suppression");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            suppressionCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            const originalLength = suppressionCompInstance.configurationSuppressionList.length;
            let suppression = suppressionMockDataService.createNewSuppression();
            suppressionCompInstance.suppressionObj = suppression;
            suppressionCompInstance.addOrUpdateSuppression();
            setTimeout(function () {
                fixture.detectChanges();
                expect(suppressionCompInstance.configurationSuppressionList.length).toEqual(originalLength + 1);
                done();
            }, 5);
        });

    });

    //rcw:comment Shreya | -Update Suppressions taking event detector object from the list updating the same
    // and calling the update function
    // from the component.
    it('Update Suppressions', function (done) {
        console.log("Update Suppression");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            suppressionCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            suppressionCompInstance.selectedIndex = suppressionCompInstance.configurationSuppressionList.length - 1;
            suppressionCompInstance.suppressionObj = _.cloneDeep(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1]);
            suppressionCompInstance.suppressionObj = suppressionMockDataService.updateSuppression(suppressionCompInstance.suppressionObj);
            suppressionCompInstance.addOrUpdateSuppression();
            setTimeout(function () {
                fixture.detectChanges();
                expect(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1].suppressionName).toEqual(suppressionCompInstance.suppressionObj.suppressionName);
                done();
            }, 5);
        });
    });

    //rcw:comment Shreya | -Publishing Suppressions sending the draft status suppressions
    // and changing the status from "DRAFT" to "PUBLISH"
    // from the component.
    it('Publish Suppressions', function (done) {
        console.log("Publish Suppression");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            suppressionCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            suppressionCompInstance.selectedIndex = suppressionCompInstance.configurationSuppressionList.length - 1;
            suppressionCompInstance.suppressionObj = _.cloneDeep(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1]);
            suppressionCompInstance.publishSuppression();
            setTimeout(function () {
                fixture.detectChanges();
                expect(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1].suppressionName).toEqual(suppressionCompInstance.suppressionObj.suppressionName);
                done();
            }, 5);
        });
    });

    //rcw:comment Shreya | -Delete Suppressions calling delete method from component
    it('Delete Suppressions', function (done) {
        console.log("Delete Suppression");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            suppressionCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            const originalLength = suppressionCompInstance.configurationSuppressionList.length;
            suppressionCompInstance.modalContent = _.cloneDeep(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1]);
            suppressionCompInstance.deleteSuppression();
            setTimeout(function () {
                fixture.detectChanges();
                expect(suppressionCompInstance.configurationSuppressionList.length).toEqual(originalLength - 1);
                done();
            }, 500);
        });
    });

    //rcw:comment Shreya | -Archive Suppression calling archive method from component
    it('Archive Suppressions', function (done) {
        console.log("Archive Suppression");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            suppressionCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            const originalLength = suppressionCompInstance.configurationSuppressionList.length;
            suppressionCompInstance.modalContent = _.cloneDeep(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1]);
            suppressionCompInstance.archiveSuppression();
            setTimeout(function () {
                fixture.detectChanges();
                expect(suppressionCompInstance.configurationSuppressionList.length).toEqual(originalLength - 1);
                done();
            }, 500);
        });
    });

    //rcw:comment Shreya | -Generating Next Version of selected Suppressions.
    it('New Version of Suppressions', function (done) {
        console.log("New Version of Suppressions");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            suppressionCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            let suppression = _.cloneDeep(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1]);
            suppressionCompInstance.generateNextVersion(suppression);
            setTimeout(function () {
                fixture.detectChanges();
                expect(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1].version).toEqual(suppression.version + 0.1);
                done();
            }, 500);
        });
    });

    //rcw:comment Shreya | -Duplicating the selected Suppression.
    it('Duplicate Suppression', function (done) {
        console.log("Duplicate Suppressions");
        fixture = TestBed.createComponent(SuppressionComponent);
        suppressionCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            suppressionCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            let suppression = _.cloneDeep(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1]);
            suppressionCompInstance.duplicateSuppression(suppression);
            setTimeout(function () {
                fixture.detectChanges();
                expect(suppressionCompInstance.configurationSuppressionList[suppressionCompInstance.configurationSuppressionList.length - 1].suppressionName).toEqual("Duplicate of Suppression - " + suppression.suppressionName);
                done();
            }, 500);
        });
    });

});


