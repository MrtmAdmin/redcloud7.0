// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    './e2e/**/*.e2e-spec.ts'
  ],
  params: {
    newUserFirstNameValue: "Test",
    newUserLastNameValue: "User",
    newUserEmailIdValue: "tuser@argusoft.in",
    upadteUserFirstNameValue: "Update",
    updateUserLastNameValue: "User"
  },
  capabilities: {
    'browserName': 'chrome'
  },
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  beforeLaunch: function() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  },
  onPrepare() {
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));

        // Login before testing any of the specs
        browser.get('/login');
        var correctUserEmail = 'julius.fernandes@emagineinternational.com';
        var correctUserPassword = 'julius';
        var userEmailField = browser.findElement(by.id('userEmail'));
        var userPasswordField = browser.findElement(by.id('userPassword'));
        var userLoginBtn = browser.findElement(by.id('loginBtn'));
        userEmailField.sendKeys(correctUserEmail);
        userPasswordField.sendKeys(correctUserPassword);
        userLoginBtn.click();
        browser.waitForAngular();
        browser.sleep(9000);
  }
};
