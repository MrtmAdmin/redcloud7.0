/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'replaceSpaces'})
export class ReplaceSpacesPipe implements PipeTransform {
    transform(value: string): any {
        if (!value) return value;
        return value.replace(/[\s]/g, '');
    }
}
