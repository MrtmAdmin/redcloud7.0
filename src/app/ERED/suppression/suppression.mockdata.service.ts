/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {Suppression} from './suppression';

export class SuppressionMockDataService {
    
    constructor(){}

    getAllSuppression() {
        let suppressionList: Suppression[] = [];
        let suppression = new Suppression();
        suppression.suppressionId = 1;
        suppression.suppressionName = "R-RECH_CHECK_TARIFF_ID_ST";
        suppression.suppressionDesc = "Suppress if TARIFF ID does not start with 100 or 200";
        suppression.failureLogLabel = "TARIF ID 100 or 200";
        suppression.successLogLabel = "TARIF ID 100 or 200";
        suppression.version = 1.20;
        suppression.status = "Draft";
        suppression.isInSimulation = false;
        suppression.isInProduction = false;
        suppression.updatable = true;
        suppression.criteriaExpression = "{\"operator\":\"AND\",\"rules\":[{\"condition\":\"=\",\"field\":\"supp add\",\"data\":\"63\"}]}";
        suppressionList.push(suppression);
        suppression = new Suppression();
        suppression.suppressionId = 2;
        suppression.suppressionName = "R_RECH_CHECL_MBB_PLAN";
        suppression.suppressionDesc = "Suppress if a mobile broadband plan";
        suppression.failureLogLabel = "Not MBB Plan";
        suppression.successLogLabel = "MBB Plan";
        suppression.version = 1.0;
        suppression.status = "Published";
        suppression.isInSimulation = false;
        suppression.isInProduction = false;
        suppression.updatable = true;
        suppression.criteriaExpression = "{\"operator\":\"AND\",\"rules\":[{\"condition\":\"=\",\"field\":\"supp add\",\"data\":\"63\"}]}";
        suppressionList.push(suppression);
        suppression = new Suppression();
        suppression.suppressionId = 3;
        suppression.suppressionName = "R_ALL_OPEN_CMP_IND";
        suppression.suppressionDesc = "Suppress if customer is in a Zoom campaign";
        suppression.failureLogLabel = "";
        suppression.successLogLabel = "In Zoom Campaign";
        suppression.version = 1.1;
        suppression.status = "Draft";
        suppression.isInSimulation = false;
        suppression.isInProduction = false;
        suppression.updatable = true;
        suppression.criteriaExpression = "{\"operator\":\"AND\",\"rules\":[{\"condition\":\"=\",\"field\":\"supp add\",\"data\":\"63\"}]}";
        suppressionList.push(suppression);
        return suppressionList;
    }

    createNewSuppression() {
        let suppression = new Suppression();
        suppression.suppressionId = null;
        suppression.suppressionName = "SUPPRESSION-ADD";
        suppression.suppressionDesc = "Suppress if TARIFF ID does not start with 100 or 200";
        suppression.failureLogLabel = "TARIF ID 100 or 200";
        suppression.successLogLabel = "TARIF ID 100 or 200";
        suppression.version = 1.20;
        suppression.status = "Published";
        suppression.isInSimulation = false;
        suppression.isInProduction = false;
        suppression.updatable = true;
        suppression.criteriaExpression = "{\"operator\":\"AND\",\"rules\":[{\"condition\":\"=\",\"field\":\"supp add\",\"data\":\"63\"}]}";
        return suppression;
    }
    
    updateSuppression(suppression:Suppression) {
        suppression.suppressionName = suppression.suppressionName+"-UPDATE";
        return suppression;
    }
    
}

