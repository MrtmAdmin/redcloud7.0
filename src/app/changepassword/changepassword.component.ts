import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {Constant} from '../common/constant';
import {UserManagementService} from '../usermanagement/usermanagement.service';
import {AuthService} from '../login/authentication.service';
import {Common} from '../common/common';

@Component({
    selector: 'change-password',
    templateUrl: './changepassword.component.html'
})

export class ChangePasswordComponent implements OnInit {

    private userName: string;
    confirmPassword: string;
    error = '';
    isRequesting: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private constant: Constant,
        private common: Common,
        private userManagementService: UserManagementService,
        private authService: AuthService
    ) {}

    ngOnInit() {
    }

    changePassword() {
        let user: any = this.common.getLoggedInUser();
        user.password = this.confirmPassword;
        let ctrl = this;
        this.isRequesting = true;

        this.userManagementService.changePassword(user.email, user.password).then(function () {
            ctrl.isRequesting = false;
            ctrl.router.navigate([ctrl.constant.dashboardUrl]);
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.error = JSON.parse(error._body).error_description;
            }
            ctrl.isRequesting = false;
        });

    }

     onClickBackButton() {
        this.router.navigate([this.constant.dashboardUrl]);
    }
}
