
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {Constant} from './constant';
import * as CryptoJS from 'crypto-js';
import {User} from '../usermanagement/user';
import {Output, EventEmitter} from '@angular/core';


export class Common {

    base64Key = CryptoJS.enc.Base64.parse("2b7e151628aed2a6abf7158809cf4f3c");
    iv = CryptoJS.enc.Base64.parse("3ad77bb40d7a3660a89ecaf32466ef97");
    private constant = new Constant();
    viewProfileDisplay: boolean = false;
    @Output() fire: EventEmitter<any> = new EventEmitter();
    @Output() profilePicFire: EventEmitter<any> = new EventEmitter();
    @Output() invalidAvtarDisplay: EventEmitter<any> = new EventEmitter();
    @Output() invalidFileDisplay: EventEmitter<any> = new EventEmitter();
    @Output() userManagementFire: EventEmitter<any> = new EventEmitter();
    constructor() {};

    public encrytStrng(strng: string) {
        if (strng != null) {
            return CryptoJS.AES.encrypt(strng, this.base64Key, {
                'iv': this.iv
            });
        }
        return null;
    }
    public decryptStrng(strng: string) {
        if (strng != null) {
            let decrypted = CryptoJS.AES.decrypt(strng, this.base64Key, {
                'iv': this.iv
            });
            return decrypted.toString(CryptoJS.enc.Utf8)
        }
        return null;

    }
    getLoggedInUser() {
        let userInfo = localStorage.getItem(this.constant.userObjName);
        let userStrng = this.decryptStrng(userInfo);
        return JSON.parse(userStrng);
    }
    getLoggedInProfilePic() {
        let userprofilePic = localStorage.getItem(this.constant.userProfilePic);
        let userprofilePicstrng = this.decryptStrng(userprofilePic);
        return userprofilePicstrng;
    }
    // rcw:comment Shreya | Check whether the user is logged In by checking the token into the local storage
    isLoggedIn() {
        var userObj = localStorage.getItem(this.constant.userObjName);
        var token = localStorage.getItem(this.constant.tokenName);
        var profilePic = localStorage.getItem(this.constant.userProfilePic);
        if (userObj != null && token != null && profilePic != undefined) {
            return true;
        } else {
            return false;
        }
    }
    //sets scroll on the field having error
    errorOnScroll() {
        console.log('error box', $('Form .error-box,.color-red').first());
        console.log('error box position', $('Form .error-box,.color-red').first().position());
        setTimeout(function () {
            $('Form').animate({
                scrollTop: $('Form .error-box,.color-red').first().position().top + 20
            }, 100);
        }, 500);
    }
    // rcw:comment Shreya | Fetch the refresh token to send to get the new access_token in oauth2
    getRefreshToken() {
        return localStorage.getItem(this.constant.refreshToken);
    }

    // rcw:comment Shreya | Fetch the access token to send to get the new access_token in oauth2
    getAccessToken() {
        return localStorage.getItem(this.constant.tokenName);
    }

    // rcw:comment Shreya |  - To check if a feature has update access or not
    checkUpdateAccess(feature: string) {
        let loggedInUserObj: User = this.getLoggedInUser();
        if ((loggedInUserObj !== null && loggedInUserObj !== undefined) && (loggedInUserObj.screenPermissions != undefined && loggedInUserObj.screenPermissions != null && loggedInUserObj.screenPermissions.length > 0)) {
            let access = "";
            for (let item of loggedInUserObj.screenPermissions) {
                if (item.screenName == feature) {
                    access = item.accessLevel;
                }
            }
            if (access != undefined && access !== null && access == this.constant.UPDATE_ACCESS) {
                return true;
            }
        }
        return false;
    }

    // rcw:comment Shreya |  - To check if a feature has update access or not
    checkReadAccess(feature: string) {
        let loggedInUserObj: User = this.getLoggedInUser();
        if ((loggedInUserObj !== null && loggedInUserObj !== undefined) && (loggedInUserObj.screenPermissions != undefined && loggedInUserObj.screenPermissions != null && loggedInUserObj.screenPermissions.length > 0)) {
            let access = "";
            for (let item of loggedInUserObj.screenPermissions) {
                if (item.screenName == feature) {
                    access = item.accessLevel;
                }
            }
            if (access != undefined && access !== null && access != this.constant.NO_PAGE_ACCESS) {
                return true;
            }
        } else {
            return false;
        }
    }

    public handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error);
    }

    winResize(menuj, paneltitlej) {
        //calculates the height of the viewport
        var viewport = $(window).height();
        //calculates the height of the header
        var header = $('#headerJ').height();
        var paneltitle = 0;
        var menu = 0;
        //calculates the height of the menu
        if (menuj != null) {
            var menu = $('#' + menuj).height();
        }
        //calculates the height of the paneltitle
        if (paneltitlej != null) {
            paneltitle = $('#' + paneltitlej).height();
        }
        var panel = viewport - header - menu - paneltitle;
        return panel;
    }

    deleteAllCookies() {
        var cookies = document.cookie.split(";");
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    }
    //sets dynamic height and width of common
    dynamicPopUpHeight() {
        var viewport = $(window).height();
        var viewwidth = $(window).width();
        var panelGot = (viewport * 8) / 10;
        var panelGot1 = (viewwidth * 9) / 10;
        var panelWarnHeight = (viewport * 6) / 10;
        var warnBoxHeight = (panelWarnHeight * 2.5) / 10;
        $('#dialogHeight').css('height', panelGot);
        $('#dialogHeight').css('width', panelGot1);
        $('.elements-panel').css('height', warnBoxHeight);
        $('.warning-element-panel').css('height', warnBoxHeight);
        $('#elementsPanelDialog').css('height', panelWarnHeight);
        $('#ruleBuilderMainDiv1').css('height', panelGot - 50);
        $('#dragAreaDiv1').css('height', panelGot - 100);
    }

    change() {
        this.fire.emit(false);
    }
    changeImageError() {
        this.invalidAvtarDisplay.emit(true);
    }
    changeFileInvalid() {
        this.invalidFileDisplay.emit(true);
    }
    getEmittedValue() {
        return this.fire;
    }
    getEmittedValueImg() {
        return this.invalidAvtarDisplay;
    }
    getEmittedValueValidImg() {
        return this.invalidFileDisplay;
    }
    changeProfilePic(imagePath: any) {
        this.profilePicFire.emit(imagePath);
    }

    getProfilePicValue() {
        return this.profilePicFire;
    }

    changeToUserManagementPage() {
        this.userManagementFire.emit(false);
    }

    getUserManagementPage() {
        return this.userManagementFire
    }

    changeingProfilePicInHeader(imagePath: any) {
        this.profilePicFire.emit(imagePath);
    }

    getProfilePicInUserManagementComponent() {
        return this.profilePicFire;
    }

    getSessionCookie() {
        var cookies = document.cookie.split(";");
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            if(name == 'sessionTimeout'){
                return true;
            }else{
                return false;
            }
        }
    }
    
    isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
}
