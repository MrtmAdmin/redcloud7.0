import { browser, element, by } from 'protractor';

export class UserManagementWebPage {
    
    getAddUserBtn() {
        return element(by.css('.add_action_btn'));
    }

    getFirstNamField() {
        return element(by.id('firstName'));
    }

    getLastNameField() {
        return element(by.id('lastName'));
    }

    getEmailField() {
        return element(by.id('userEmail'));
    }


    getSaveBtn() {
        return element(by.id('saveBtn'));
    }

    getRole1CheckBox(){
      return  element(by.css("label[for='userRole0']"));
    }

    getRole2CheckBox(){
      return  element(by.css("label[for='userRole1']"));
    }

    getEditBtn(){
      return  element(by.css('.red-btn'));
    }

}